<div class="container">
<div class="h-600x h-sm-auto">
<div class="h-2-3 h-sm-auto oflow-hidden">

<div class="pb-5 pr-5 pr-sm-0 float-left float-sm-none w-2-3 w-sm-100 h-100 h-sm-300x">
    <?php
    $all_posts = Post::get_posts(0, 1);
    foreach ($all_posts as $all_p)
    {
        ?>    
    <a class="pos-relative h-100 dplay-block" href="<?php BASE_URL?>single_post.php?post_id=<?php echo($all_p->id);?>">

                <div class="img-bg bg-1 bg-grad-layer-6"></div>
                <img style="width:100%; height: 100%" src="<?php  echo($all_p->post_image);?>" alt="">
                <div class="abs-blr color-white p-20 bg-sm-color-7">
                        <h3 class="mb-15 mb-sm-5 font-sm-13"><b><?php echo($all_p->post_name);?></b></h3>
                        <ul class="list-li-mr-20">
                                <li>by <span class="color-primary"><b><?php echo($all_p->author_name);?></b></span> <?php echo($all_p->post_date);?></li>
                                <li><i class="color-primary mr-5 font-12 ion-ios-bolt"></i><?php echo($all_p->post_views)?></li>
                                <li><i class="color-primary mr-5 font-12 ion-chatbubbles"></i><?php echo($all_p->post_comments)?></li>
                        </ul>
                </div><!--abs-blr -->
        </a><!-- pos-relative -->
        <?php
        }
    ?>
       
</div><!-- w-1-3 -->

<div class="float-left float-sm-none w-1-3 w-sm-100 h-100 h-sm-600x">
<?php
$right_posts = Post::get_Posts(1, 2);
foreach ($right_posts as $rp)
{
    ?>
    
        <div class="pl-5 pb-5 pl-sm-0 ptb-sm-5 pos-relative h-50">
                <a class="pos-relative h-100 dplay-block" href="http://localhost:8080/erozgaar/technews/single_post.php?post_id=<?php echo($rp->id);?>">

                        <div class="img-bg bg-2 bg-grad-layer-6"></div>
                <img style="width:100%; height: 100%" src="<?php echo($rp->post_image);?>" alt="">
                        <div class="abs-blr color-white p-20 bg-sm-color-7">
                                <h4 class="mb-10 mb-sm-5"><b><?php echo($rp->post_name);?></b></h4>
                                <ul class="list-li-mr-20">
                                        <li><?php echo($rp->post_date);?></li>
                                        <li><i class="color-primary mr-5 font-12 ion-ios-bolt"></i><?php echo($rp->post_comments);?></li>
                                        <li><i class="color-primary mr-5 font-12 ion-chatbubbles"></i><?php echo($rp->post_views);?></li>
                                </ul>
                        </div><!--abs-blr -->
                </a><!-- pos-relative -->
        </div><!-- w-1-3 -->
        <?php
}
?>
</div><!-- float-left -->

</div><!-- h-2-3 -->

<div class="h-1-3 oflow-hidden">
<?php
$bottom_posts = Post::get_Posts(1, 3);
foreach ($bottom_posts as $bp)
{
?>
<div class="pr-5 pr-sm-0 pt-5 float-left float-sm-none pos-relative w-1-3 w-sm-100 h-100 h-sm-300x">
        <a class="pos-relative h-100 dplay-block" href="http://localhost:8080/erozgaar/technews/single_post.php?post_id=<?php echo($bp->id);?>">

                <div class="img-bg bg-4 bg-grad-layer-6"></div>
                <img style="width:100%; height: 100%" src="<?php echo($bp->post_image);?>" alt="">
                <div class="abs-blr color-white p-20 bg-sm-color-7">
                        <h4 class="mb-10 mb-sm-5"><b><?php echo($bp->post_name);?></b></h4>
                        <ul class="list-li-mr-20">
                                <li><?php echo($bp->post_date);?></li>
                                <li><i class="color-primary mr-5 font-12 ion-ios-bolt"></i><?php echo($rp->post_comments);?></li>
                                <li><i class="color-primary mr-5 font-12 ion-chatbubbles"></i><?php echo($rp->post_views);?></li>
                        </ul>
                </div><!--abs-blr -->
        </a><!-- pos-relative -->
</div><!-- w-1-3 -->
 <?php
}
?>

</div><!-- h-2-3 -->
</div><!-- h-100vh -->
</div><!-- container -->