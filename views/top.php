<?php
//require_once '../models/Post.php';
define("BASE_FOLDER", "/technews/");
define("BASE_URL", $_SERVER['HTTP_HOST'] . BASE_FOLDER);
//echo(BASE_URL);
//die;
?>
<!DOCTYPE HTML>
<html lang="en">
<head>
	<title>TechNews</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8">
	
	<!-- Font -->
	<link href="https://fonts.googleapis.com/css?family=Encode+Sans+Expanded:400,600,700" rel="stylesheet">
	
	<!-- Stylesheets -->
	
	<link href="plugin-frameworks/bootstrap.css" rel="stylesheet">
	
	<link href="fonts/ionicons.css" rel="stylesheet">
		
	<link href="common/styles.css" rel="stylesheet">
        
        