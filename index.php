<?php
require_once './tn-admin/models/Post.php';
require_once 'views/top.php';
?>
</head>
<body>
	
<?php
require_once 'views/header.php';
require_once 'views/main-top.php';
?>	<section>
<div class="container">
<div class="row">

<div class="col-md-12 col-lg-8">
<?php
require_once 'views/recent-news.php';
require_once 'views/mining-news.php';
?>
 
</div><!-- col-md-9 -->

<div class="d-none d-md-block d-lg-none col-md-3"></div>
<div class="col-md-6 col-lg-4">
<div class="pl-20 pl-md-0">
<?php

require_once 'views/popular-posts.php';
require_once 'views/bit-rate.php';
require_once 'views/mobile-app.php';
require_once 'views/newsletter.php';
?>
</div><!--  pl-20 -->
</div><!-- col-md-3 -->

</div><!-- row -->
</div><!-- container -->
	</section>
<?php
require_once 'views/footer.php';