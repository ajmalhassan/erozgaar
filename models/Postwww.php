<?php
require_once 'DB_Connection.php';
class Post extends DB_Connection {
    private $id;
    private $post_name;
    private $post_description;
    private $post_category;
    private $post_image;
    private $post_date;
    private $author_name;
    private $post_comments;
    private $post_views;
    private $post_tags;

    public function __set($name, $value) {
        $method_name = "set_$name";
        if(!method_exists($this, $method_name))
        {
            throw new Exception("Set Property $name does not exist");
        }
        $this->$method_name($value);
    }
    public function __get($name) {
        $method_name = "get_$name";
        if(!method_exists($this, $method_name))
        {
            throw new Exception("Get Property $name does not exist");
        }
        return $this->$method_name();
    }
    private function set_id($id)
    {
            if(!is_numeric($id) || $id <=0)
            {
                throw new Exception("*Invalid/Missing post ID");
            }
            $this->id = $id;
    }
    private function get_id()
    {
        return $this->id;
    }
    private function set_post_name($post_name)
    {
        $reg = "/^[a-z]/";
        if(!preg_match($reg, $post_name))
        {
            throw new Exception("*Invalid Post Name");
        }
        $this->post_name = $post_name;
    }
    private function get_post_name()
    {
        return $this->post_name;
    }
   
    private function set_post_description($description)
    {
        $description = trim($description);
        
        if(!$description)
        {
            throw new Exception("*Invalid Post description");
        }
        $this->post_description = $description;
    }
    private function get_post_description()
    {
        return $this->post_description;
    }
     private function set_post_category($post_category)
    {
        $reg = "/^[a-zA-Z0-9. ]+$/i";
        if(!preg_match($reg, $post_category))
        {
            throw new Exception("*Invalid Post category");
        }
        $this->post_category = $post_category;
    }
    private function get_post_category()
    {
        return $this->post_category;
    }
    private function get_post_date()
    {
        return $this->post_date;
    }
    private function set_post_image($post_image)
    {
        
        $this->post_image = $post_image;
    }
    private function get_post_image()
    {
        return $this->post_image;
    }
    private function set_author_name($author_name)
    {
        $req = "/^[a-zA-Z ]+$/i";
        if(!preg_match($req, $author_name))
        {
            throw new Exception("*Invalid Author Name");
        }
        $this->author_name = $author_name;
    }
    private function get_author_name()
    {
        return $this->author_name;
    }
    private function set_post_comments($post_comments)
    {
        if(!is_numeric($post_comments) || $post_comments <=0)
        {
            throw new Exception("*Comment Must be integer");
        }
        $this->post_comments = $post_comments;
    }
    private function get_post_comments()
    {
        return $this->post_comments;
    }
    private function set_post_views($post_views)
    {
        if(!is_numeric($post_views) || $post_views <=0)
        {
            throw new Exception("*Views Must be integer");
        }
        $this->post_views = $post_views;
    }
    private function get_post_views()
    {
        return $this->post_views;
    }
     private function set_post_tags($post_tags)
    {
         $reg = "/[a-zA-Z0-9., ]+$/i";
        if(!preg_match($reg, $post_tags))
        {
            throw new Exception("*Must be enter tags");
        }
        $this->post_tags = $post_tags;
    }
    private function get_post_tags()
    {
        return $this->post_tags;
    }

    public static function get_posts($start, $limit)
    {
        $obj_db = self::obj_db();
    if($start || $limit)
        {
         $query = "select * from posts order by id asc limit $start, $limit";
        }
        
        $result = $obj_db->query($query);
        $posts = array();
        while($post = $result->fetch_object())
        {
            $temp = new Post();
            $temp->id = $post->id;
            $temp->post_name = $post->post_name;
            $temp->post_description = $post->post_description;
            $temp->post_image = $post->post_image;
            $temp->post_date = $post->date;
            $temp->author_name = $post->author_name;
            $temp->post_comments = $post->comments;
            $temp->post_views = $post->views;
            $temp->post_image = $post->post_image;
            $posts[] = $temp;
        }
        return $posts;
    }
    public static function load_more_post()
    {
        $obj_db = self::obj_db();
        $perPagePost = 4;
        //Counting total posts
//        $allCountQuery = "select count(*) as allCount from posts where cat_id = 1";
//        $allCountResult = $obj_db->query($allCountQuery);
//        $allCountFetch = $allCountResult->fetch_object();
        //$allCount = $allCountFetch['allCount'];
        //$query = "select * from posts where cat_id = 1 limit 0, $perPagePost";
//        $query = " SELECT *
//                FROM posts p
//                INNER JOIN categories c ON c.id = p.cat_id
//                WHERE c.cat_name = 'category 2' ";
        $query = "select * from posts where cat_id = 1";
        $result = $obj_db->query($query);
        $loadPost = array();
        while($p = $result->fetch_object())
        {
            $temp =  new Post();
            $temp->id = $p->id;
            $temp->post_name = $p->post_name;
            $temp->post_description = $p->post_description;
            $temp->post_image = $p->post_image;
            $temp->post_date = $p->date;
            $temp->author_name = $p->author_name;
            $temp->post_comments = $p->comments;
            $temp->post_views = $p->views;
            $temp->post_image = $p->post_image;
            $loadPost[] = $temp;
        }
        return $loadPost;
    }
    public static function get_single_post($post_id)
    {
      
        $obj_db = self::obj_db();
        $query_select = "select * from posts where id = $post_id";
//        echo($query_select);
//        exit();
        $result = $obj_db->query($query_select);
        
        $single_post = array();
        while($post = $result->fetch_object())
        {
            $temp = new Post();
            $temp->id = $post->id;
            $temp->post_name = $post->post_name;
            $temp->post_description = $post->post_description;
            $temp->post_image = $post->post_image;
            $temp->post_comments = $post->comments;
            $temp->post_views = $post->views;
            $temp->author_name = $post->author_name;
            $single_post[] = $temp;
        }
        return $single_post;
        
    }
    
    
    public function add_post()
    {
        $current_time = time();
        $now = date("Y-m-d",strtotime($current_time));
        $obj_db = $this->obj_db();
        $query_insert = "INSERT INTO `posts` (`id`, `post_name`, `post_description`, `post_image`, `date`, `author_name`, `views`, `comments`, `cat_id`) "
                . " VALUES (NULL, '$this->post_name', '$this->post_description', '$this->post_image', '$now', '$this->author_name', 2000, 100, 1 );";
        $result = $obj_db->query($query_insert);
        if($obj_db->errno)
        {
            throw new Exception("Post not inserted into database $obj_db->error - $obj_db->errno");
        }
    }
    public static function get_categories()
    {
        $obj_db = $this->obj_db();
        $query_select = "select * from categories";
        $result = $obj_db->query($query_select);
        $data = $result->fetch_object();
       
    }
}
