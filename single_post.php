<?php
require_once '../technews/tn-admin/models/Post.php';
require_once 'views/top.php';
?>
</head>
<body>
	
<?php
require_once 'views/header.php';
?>	
  
	<section class="ptb-0">
		<div class="mb-30 brdr-ash-1 opacty-5"></div>
		<div class="container">
			<a class="mt-10" href="index.html"><i class="mr-5 ion-ios-home"></i>Home<i class="mlr-10 ion-chevron-right"></i></a>
			<a class="mt-10 color-ash" href="archive-page.html">Blog Archive</a>
		</div><!-- container -->
	</section>
<section>
<div class="container">
<div class="row">

<div class="col-md-12 col-lg-8">
    <?php
    if(isset($_GET['post_id']))
    {
        $post_id  =$_GET['post_id'];
        $single_post = Post::get_single_post($post_id);
        foreach ($single_post as $p)
        {
            ?>
    
        <img src="<?php echo($p->post_image);?>" alt="">
        <h3 class="mt-30"><b><?php echo($p->post_name);?></b></h3>
        <ul class="list-li-mr-20 mtb-15">
                <li>by <a href="#"><b><?php echo($p->author_name);?> </b></a> <?php echo($p->post_date);?></li>
                <li><i class="color-primary mr-5 font-12 ion-ios-bolt"></i><?php echo($p->post_views); ?></li>
                <li><i class="color-primary mr-5 font-12 ion-chatbubbles"></i><?php echo($p->post_comments); ?></li>
        </ul>

        <?php echo($p->post_description);?>
        <?php
            
        }
    }
    ?>
        <div class="float-left-right text-center mt-40 mt-sm-20">

                <ul class="mb-30 list-li-mt-10 list-li-mr-5 list-a-plr-15 list-a-ptb-7 list-a-bg-grey list-a-br-2 list-a-hvr-primary ">
                        <li><a href="#">MULTIPURPOSE</a></li>
                        <li><a href="#">FASHION</a></li>
                        <li class="mr-0"><a href="#">BLOGS</a></li>
                </ul>
                <ul class="mb-30 list-a-bg-grey list-a-hw-radial-35 list-a-hvr-primary list-li-ml-5">
                        <li class="mr-10 ml-0">Share</li>
                        <li><a href="#"><i class="ion-social-facebook"></i></a></li>
                        <li><a href="#"><i class="ion-social-twitter"></i></a></li>
                        <li><a href="#"><i class="ion-social-google"></i></a></li>
                        <li><a href="#"><i class="ion-social-instagram"></i></a></li>
                </ul>

        </div><!-- float-left-right -->

        <div class="brdr-ash-1 opacty-5"></div>

        <h4 class="p-title mt-50"><b>YOU MAY ALSO LIKE</b></h4>
        <div class="row">

                <div class="col-sm-6">
                        <img src="images/crypto-news-2-600x450.jpg" alt="">
                        <h4 class="pt-20"><a href="#"><b>2017 Market Performance: <br/>Crypto vs.Stock</b></a></h4>
                        <ul class="list-li-mr-20 pt-10 mb-30">
                                <li class="color-lite-black">by <a href="#" class="color-black"><b>Olivia Capzallo,</b></a>
                                Jan 25, 2018</li>
                                <li><i class="color-primary mr-5 font-12 ion-ios-bolt"></i>30,190</li>
                                <li><i class="color-primary mr-5 font-12 ion-chatbubbles"></i>47</li>
                        </ul>
                </div><!-- col-sm-6 -->

                <div class="col-sm-6">
                        <img src="images/crypto-news-1-600x450.jpg" alt="">
                        <h4 class="pt-20"><a href="#"><b>2017 Market Performance: Crypto vs.Stock</b></a></h4>
                        <ul class="list-li-mr-20 pt-10 mb-30">
                                <li class="color-lite-black">by <a href="#" class="color-black"><b>Olivia Capzallo,</b></a>
                                Jan 25, 2018</li>
                                <li><i class="color-primary mr-5 font-12 ion-ios-bolt"></i>30,190</li>
                                <li><i class="color-primary mr-5 font-12 ion-chatbubbles"></i>47</li>
                        </ul>
                </div><!-- col-sm-6 -->

        </div><!-- row -->

        <h4 class="p-title mt-20"><b>03 COMMENTS</b></h4>
        
        <div id="disqus_thread"></div>
<script>

/**
*  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
*  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
/*
var disqus_config = function () {
this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
};
*/
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://httplocalhost8080erozgaartechnews.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                            

<!--        <div class="sided-70 mb-40">

                <div class="s-left rounded">
                        <img src="images/profile-3-120x120.jpg" alt="">
                </div> s-left 

                <div class="s-right ml-100 ml-xs-85">
                        <h5><b>Shuhein Chui, </b> <span class="font-8 color-ash">Nov 21, 2017</span></h5>
                        <p class="mtb-15">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium 
                                doloremque laudantium, totam rem aperiam.</p>
                        <a class="btn-brdr-grey btn-b-sm plr-15 mr-10 mt-5" href="#"><b>LIKE</b></a>
                        <a class="btn-brdr-grey btn-b-sm plr-15 mt-5" href="#"><b>REPLY</b></a>
                </div> s-right 

        </div> sided-70 

        <div class="sided-70 ml-100 ml-xs-20 mb-40">

                <div class="s-left rounded">
                        <img src="images/profile-1-120x120.jpg" alt="">
                </div> s-left 

                <div class="s-right ml-100 ml-xs-85">
                        <h5><b>Shuhein Chui, </b> <span class="font-8 color-ash">Nov 21, 2017</span></h5>
                        <p class="mtb-10">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium 
                                doloremque laudantium, totam rem aperiam.</p>
                        <a class="btn-brdr-grey btn-b-sm plr-15 mr-10 mt-5" href="#"><b>LIKE</b></a>
                        <a class="btn-brdr-grey btn-b-sm plr-15 mt-5" href="#"><b>REPLY</b></a>
                </div> s-right 

        </div> sided-70 

        <div class="sided-70 mb-50">

                <div class="s-left rounded">
                        <img src="images/profile-2-120x120.jpg" alt="">
                </div> s-left 

                <div class="s-right ml-100 ml-xs-85">
                        <h5><b>Shuhein Chui, </b> <span class="font-8 color-ash">Nov 21, 2017</span></h5>
                        <p class="mt-10 mb-15">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium 
                                doloremque laudantium, totam rem aperiam.</p>
                        <a class="btn-brdr-grey btn-b-sm plr-15 mr-10" href="#"><b>LIKE</b></a>
                        <a class="btn-brdr-grey btn-b-sm plr-15" href="#"><b>REPLY</b></a>
                </div> s-right 

        </div> sided-70 

        <h4 class="p-title mt-20"><b>LEAVE A COMMENT</b></h4>

        <form class="form-block form-plr-15 form-h-45 form-mb-20 form-brdr-lite-white mb-md-50">
                <input type="text" placeholder="Your Name*:">
                <input type="text" placeholder="Your Email*:">
                <input type="text" placeholder="Your Phone*:">
                <textarea class="ptb-10" placeholder="Your Comment"></textarea>
                <button class="btn-fill-primary plr-30" rows="4" cols="50" type="submit"><b>LEAVE A COMMENT</b></button>
        </form>-->
</div><!-- col-md-9 -->
				
				<div class="d-none d-md-block d-lg-none col-md-3"></div>
				<div class="col-md-6 col-lg-4">
					<div class="pl-20 pl-md-0">
<!--						<ul class="list-block list-li-ptb-15 list-btm-border-white bg-primary text-center">
							<li><b>1 BTC = $13,2323</b></li>
							<li><b>1 BCH = $13,2323</b></li>
							<li><b>1 ETH = $13,2323</b></li>
							<li><b>1 LTC = $13,2323</b></li>
							<li><b>1 DAS = $13,2323</b></li>
							<li><b>1 BCC = $13,2323</b></li>
						</ul>-->
						
						<div class="mtb-50">
					<?php
require_once 'views/popular-posts.php';
                                        ?>
                </div><!-- mtb-50 -->
<?php
require_once 'views/mobile-app.php';
?>

                <div class="mtb-50 mb-md-0">
                        <h4 class="p-title"><b>NEWSLETTER</b></h4>
                        <p class="mb-20">Subscribe to our newsletter to get notification about new updates,
                                information, discount, etc..</p>
                        <form class="nwsltr-primary-1">
                                <input type="text" placeholder="Your email"/>
                                <button type="submit"><i class="ion-ios-paperplane"></i></button>
                        </form>
                </div><!-- mtb-50 -->

        </div><!--  pl-20 -->
</div><!-- col-md-3 -->

</div><!-- row -->

</div><!-- container -->
</section>
<?php
require_once 'views/footer.php';