<?php
require_once 'DB_Connection.php';
class Category extends DB_Connection {
    private $id;
    private $category_name;
    private $cat_date;




    public function __set($name, $value) {
        $method_name = "set_$name";
        if(!method_exists($this, $method_name))
        {
            throw new Exception("SET propert $name does not exist");
        }
        $this->$method_name($value);
    }
    public function __get($name) {
        $method_name = "get_$name";
        if(!method_exists($this, $method_name))
        {
            throw new Exception("GET property $name does not exist");
        }
        return $this->$method_name();
    }
    private function set_id($id)
    {
        if(!is_numeric($id) || $id <=0)
        {
            throw new Exception("* ID must be numeric");
        }
        $this->id= $id;
    }
    private function get_id()
    {
        return $this->id;
    }
    private function set_category_name($category_name)
    {
        $reg = "/^[a-zA_Z0-9 ]+$/i";
        if(!preg_match($reg, $category_name))
        {
            throw new Exception("*Category invalid");
        }
        $this->category_name = $category_name;
    }
    private function get_category_name()
    {
        return $this->category_name;
    }
    
    private function get_cat_date()
    {
        return $this->cat_date;
    }

    public function add_category()
    {
        $now = date('Y-m-d');
        $obj_db = $this->obj_db();
        $query_insert = "INSERT INTO `categories` (`id`, `cat_name`, `cat_date`) VALUES (NULL, '$this->category_name', '$now')";
//        echo($query_insert);
//        die;
        $result = $obj_db->query($query_insert);
        if($obj_db->errno)
        {
            throw new Exception("Category added error $obj_db->errno - $obj_db->error");
        }
        
    }
    public static function get_categories()
    {
        $obj_db = self::obj_db();
        $query_select = "select * from categories";
        $result = $obj_db->query($query_select);
        $cat = array();
        while($data = $result->fetch_object())
        {
            $temp = new Category();
            $temp->id = $data->id;
            $temp->cat_date = $data->cat_date;
            $temp->category_name = $data->cat_name;
            $cat[] = $temp;
        }
        return $cat;
       
    }
    
    public function remove_cat($key)
    {
        $obj_db = $this->obj_db();
        $query_delete = "delete from categories where id = '$key' ";
        $result = $obj_db->query($query_delete);
        
        if(!$obj_db->affected_rows)
        {
            throw new Exception("Category not deleted");
        }
    }
    public function update_cat($edit_id)
    {
        $obj_db = $this->obj_db();
        $query_update = "UPDATE `categories` SET `cat_name` = '$this->category_name' WHERE `categories`.`id` = $edit_id";
        $obj_db->query($query_update);
    }
    public static function get_single_category($id)
    {
        $obj_db = self::obj_db();
        $query_select = "select * from categories where id = $id";
//        echo $query_select;
//        die;
        $result = $obj_db->query($query_select);
        $cat = array();
        while($data = $result->fetch_object())
        {
            $temp = new Category();
            $temp->category_name = $data->cat_name;
            $cat[] = $temp;
        }
        return $cat;
       
        
    }
}
