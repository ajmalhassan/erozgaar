<?php
class DB_Connection
{
    public function obj_db()
    {
        $host = "localhost";
        $user = "root";
        $password = "";
        $database = "technews";
        $obj_db  = new mysqli();
        $obj_db->connect($host, $user, $password, $database);
        if($obj_db->connect_errno)
        {
            throw new Exception("DB Connect Error ---> $obj_db->connect_error ---> $obj_db->connect_errno");
        }
        $obj_db->select_db($database);
        if($obj_db->errno)
        {
            throw new Exception("Database selected error $obj_db->connect_error ---> $obj_db->errno");
        }
        return $obj_db;
        
    }
}

