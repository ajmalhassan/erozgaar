<?php
//session_start();
require_once 'DB_Connection.php';
class User extends DB_Connection {
    private $id;
    private $email;
    private $password;
    private $login_status;


    public function __set($name, $value) {
        $method_name = "set_$name";
        if(!method_exists($this, $method_name))
        {
            throw new Exception("Set property $name does not exist");
        }
        $this->$method_name($value);
    }
    public function __get($name) {
        $method_name = "get_$name";
        if(!method_exists($this, $method_name))
        {
            throw new Exception("Get property $name does not exist");
        }
        return $this->$method_name();
    }

    private function set_id($id)
    {
        if(!is_numeric($id) || $id <=0)
        {
            throw new Exception("*ID must be numeric");
        }
        $this->id = $id;
    }
    private function get_id()
    {
        return $this->id;
    }

    private function set_email($email)
    {
        $reg = "/^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zAZ]\.)+[a-zA-Z]{2,4})$/";
        if(!preg_match($reg, $email))
        {
            throw new Exception("*Invalid Email");
        }
        $this->email = $email;
    }
    private function get_email()
    {
        return $this->email;
    }
    private function set_password($password)
    {
     $reg = "/^[a-z0-9 ][A-Z0-9 ]+$/i";
     if(!preg_match($reg, $password))
     {
         throw new Exception("*Invalid Password");
     }
     $this->password = $password;
    }
    private function get_password()
    {
        return $this->password;
    }
    private function get_login()
    {
        return $this->login_status;
    }

    public function login($remember = FALSE)
    {
        //$obj_db = $this->obj_db();
        $obj_db = $this->obj_db();
        $query_select = "select id, email, password from users where "
                . " email = '$this->email' "
                . " and password = '$this->password' ";
//        echo($query_select);
//        die;
                $_SESSION['id'] = $this->id;
        $result = $obj_db->query($query_select);
        if($obj_db->errno)
        {
            throw new Exception("Select login user error $obj_db->error - $obj_db->errno");
        }
        if(!$result->num_rows)
        {
            throw new Exception("Login falied");
        }
        $data = $result->fetch_object();
        $this->id = $data->id;
        $this->email = $data->email;
        $this->password = $data->password;
        $this->login_status = TRUE;
        $str_user = serialize($this);
        $_SESSION['obj_user'] = $str_user;
        
        if($remember)
        {
            $expiry_time = time() + (60 * 60 * 24 * 15);
            setcookie('technews', $str_user, $expiry_time);
        }
        
    }  
    
    public function logout()
    {
        if(isset($_SESSION['obj_user']))
        {
            unset($_SESSION['obj_user']);
        }
    }
    
}
