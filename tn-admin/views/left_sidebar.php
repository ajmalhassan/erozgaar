<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index.php" class="brand-link">
      <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Admin TechNews</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="dist/img/profileimage.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Ajmal hassan</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          
          <li class="nav-item">
              <a href="insert_post.php" class="nav-link">
              <i class="nav-icon fa fa-plus-square"></i>
              <p>
                Add Post
              </p>
            </a>
          </li>
          <li class="nav-item">
              <a href="posts.php" class="nav-link">
              <i class="nav-icon fa fa-folder"></i>
              <p>
                All Post
              </p>
            </a>
          </li>
          <li class="nav-item">
              <a href="categories.php" class="nav-link">
              <i class="nav-icon fa fa-th"></i>
              <p>
                Categories
              </p>
            </a>
          </li>
          <li><hr></li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-cogs"></i>
              <p>
                Setting
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="profile.php" class="nav-link">
                  <i class="fa fa-user"></i>
                  <p>Profile</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="fa fa-users"></i>
                  <p>Users</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="fa fa-cog"></i>
                  <p>Tech News Setting</p>
                </a>
              </li>
            </ul>
          </li>
          
          <?php
          if($obj_user->login)
          {
              ?>
          
          <li class="nav-item">
              <a href="controller/logout_process.php" class="nav-link">
              <i class="nav-icon fa fa-th"></i>
              <p>
                Logout
              </p>
            </a>
          </li>
          <?php
          }
          else{
              ?>
          <li class="nav-item">
              <a href="login.php" class="nav-link">
              <i class="nav-icon fa fa-th"></i>
              <p>
                Login
              </p>
            </a>
          </li>
          <?php
          }
          ?>
          
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>