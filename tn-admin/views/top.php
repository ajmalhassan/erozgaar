<?php
session_start();
require_once 'models/User.php';
define("BASE_FOLDER", "/erozgaar/technews/tn-admin/");
define("BASE_URL", "http://" . $_SERVER['HTTP_HOST']. BASE_FOLDER);
//echo(BASE_URL);
//die;
if(isset($_SESSION['id']))
{
    $id = $_SESSION['id'];
}
if(isset($_COOKIE['technews']) && !isset($_SESSION['user_object']))
{
    $_SESSION['obj_user'] = $_COOKIE['technews'];
}
if(isset($_SESSION['obj_user']))
{
    $obj_user = unserialize($_SESSION['obj_user']);
}
else
{
    $obj_user = new User();
}

$current = $_SERVER['PHP_SELF'];
//echo($current);

$public_pages = array(
    BASE_FOLDER . "login.php",
    BASE_FOLDER . "register.php",
);
//echo $public_pages[0];
$restricted_pages = array(
    BASE_FOLDER . "index.php",
    BASE_FOLDER . "categories.php",
    BASE_FOLDER . "insert_post.php",
    BASE_FOLDER . "msg.php",
    BASE_FOLDER . "profile.php",
    BASE_FOLDER . "update_post.php"
);

if($obj_user->login && in_array($current, $public_pages)){
    $_SESSION['msg'] = "You must logout to view this page";
    
    header("Location:" . BASE_URL . "msg.php");
}

if(!$obj_user->login && in_array($current, $restricted_pages)){
    //$_SESSION['msg'] = "You must login to view this page";
    header("Location:" . BASE_URL . "login.php");
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminTechNews | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker-bs3.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link href="dist/css/custom.css" rel="stylesheet" type="text/css"/>