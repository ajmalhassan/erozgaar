<?php
require_once 'views/top.php';
?>
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="index.php"><b>Admin</b>TechNews</a>
  </div>
  <!-- /.login-logo -->
  <?php
       if(isset($_SESSION['errors']))
       {
           $error = $_SESSION['errors'];
           unset($_SESSION['errors']);
       }
       if(isset($_SESSION['obj_user']))
       {
           $obj_user = unserialize($_SESSION['obj_user']);
       }
       else
       {
           $obj_user = new User();
       }
      ?>
  <div class="card">
    <div class="card-body login-card-body">
        <?php
        if(isset($_SESSION['msg']))
        {
            $msg = $_SESSION['msg'];
            echo ($msg);
            unset($_SESSION['msg']);
        }
        ?>
      <p class="login-box-msg">Sign in to start your session</p>
      
      <form action="controller/login_process.php" method="post">
        <div class="form-group has-feedback">
          <input type="email" class="form-control" name="email" id="email" placeholder="Email">
          <span class="fa fa-envelope form-control-feedback"> 
          <?php
          if(isset($error['email']))
          {
              echo ($error['email']);
          }
          ?>
          </span>
        </div>
        <div class="form-group has-feedback">
          <input type="password" class="form-control" name="password" id="password" placeholder="Password">
          <span class="fa fa-lock form-control-feedback">
              <?php
              if(isset($error['password']))
              {
                  echo($error['password']);
              }
              ?>
          </span>
        </div>
        <div class="row">
          <div class="col-8">
            <div class="checkbox icheck">
              <label>
                <input type="checkbox"> Remember Me
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <div class="social-auth-links text-center mb-3">
        <p>- OR -</p>
        <a href="#" class="btn btn-block btn-primary">
          <i class="fa fa-facebook mr-2"></i> Sign in using Facebook
        </a>
        <a href="#" class="btn btn-block btn-danger">
          <i class="fa fa-google-plus mr-2"></i> Sign in using Google+
        </a>
      </div>
      <!-- /.social-auth-links -->

      <p class="mb-1">
        <a href="#">I forgot my password</a>
      </p>
      <p class="mb-0">
        <a href="register.php" class="text-center">Register a new membership</a>
      </p>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src=".plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- iCheck -->
<script src="plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass   : 'iradio_square-blue',
      increaseArea : '20%' // optional
    })
  })
</script>
<script>
//      function homePage()
//      {
//            var email = document.getElementById("email").value;
//            var password = document.getElementById("password").value;
//            if(email == "" || password == "")
//            {
//                alert("Email and Password required");
//                return;
//            }
//            else
//            {
//                window.location = 'index.php';
//            }
//      }
  </script>
</body>
</html>
