<?php
session_start();
require_once '../models/User.php';
$obj_user = unserialize($_SESSION['obj_user']);
try {
    $obj_user->logout();
} catch (Exception $ex) {
    $_SESSION['msg'] = $ex->getMessage();
}
header("Location:../login.php");


