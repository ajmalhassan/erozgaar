<?php
session_start();
require_once '../models/Category.php';
$obj_cat = new Category();
$errors = array();

try {
    $obj_cat->category_name = $_POST['category_name'];
} catch (Exception $ex) {
    $errors['category_name'] = $ex->getMessage();
}
if(count($errors) == 0)
{
    try {
        $edit_id = $_GET['edit_id'];
        $obj_cat->update_cat($edit_id);
        $obj_cat->add_category();
        $msg = "<span class='alert alert-success' style='padding:0!important; padding-left:10px!important; padding-right:10px!important; font-size:12px;'>Category successfully created</span>";
        $_SESSION['msg'] = $msg;
        header("Location:../categories.php");
    } catch (Exception $ex) {
        echo $ex->getMessage();
    }
}
else
{
        $msg = "<span class='alert alert-success' style='padding:0!important; padding-left:10px!important; padding-right:10px!important; font-size:12px;'>Category not created</span>";
        $_SESSION['msg'] = $msg;
        header("Location:../categories.php");
        $_SESSION['obj_cat'] = serialize($obj_cat);
        $_SESSION['errors'] = $errors;
}
