<?php
//echo("dfksdlf");
//die;
session_start();
require_once '../models/User.php';
$obj_user = new User();

$errors = array();
try {
     $obj_user->email = $_POST['email'];
} catch (Exception $ex) {
    $errors['email'] = $ex->getMessage();
}
try {
    $obj_user->password = $_POST['password'];
} catch (Exception $ex) {
    $errors['password'] = $ex->getMessage();
}
if(count($errors) == 0)
{
    try {
        $remember = isset($_POST['remember']) ? TRUE : FALSE;
        $obj_user->login($remember);
        header("Location:../index.php");
    } catch (Exception $ex) {
        $_SESSION['msg'] = $ex->getMessage();
        header("Location:../login.php");
    }
}
else
{
    $msg = "Check user errors";
    $_SESSION['msg'] = $msg;
    $_SESSION['errors'] = $errors;
    $_SESSION['obj_user'] = serialize($obj_user);
    header("Location:../login.php");
}



