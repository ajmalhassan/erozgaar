<?php
session_start();
require_once '../models/Category.php';
require_once '../models/Post.php';

if(isset($_SESSION['obj_cat']))
{
    $obj_cat = unserialize($_SESSION['obj_cat']);
}
else
{
    $obj_cat = new Category();
}
if(isset($_SESSION['obj_post']))
{
    $obj_post = unserialize($_SESSION['obj_post']);
}
else{
    $obj_post = new Post();
}

if(isset($_GET['action']))
{
//    echo ($_GET['action']);
//    echo ($_GET['key']);
//    die;
    switch ($_GET['action'])
    {
        case "remove_cat":
            $key = $_GET['key'];
            if(!$obj_cat->remove_cat($key))
            {
                $_SESSION['rem_msg'] = "<span class='alert alert-success'>Category successfully deleted</span>";
            }
            break;
        case "remove_post":
            $key = $_GET['post_key'];
            if(!$obj_post->remove_post($key))
            {
               $_SESSION['post_msg'] = "<span class='alert alert-success'>Post successfully deleted</span>";
            }
    }
}
$_SESSION['obj_cat'] = serialize($obj_cat);
header("Location:" . $_SERVER['HTTP_REFERER']);
        