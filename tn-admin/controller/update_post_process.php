<?php
session_start();
require_once '../models/Post.php';
$obj_post = new Post();
$errors = array();

try {
    $obj_post->post_name = $_POST['post_name'];
} catch (Exception $ex) {
    $errors['post_name'] = $ex->getMessage();
}

try {
    $obj_post->post_description = $_POST['post_description'];
} catch (Exception $ex) {
    $errors['post_description'] = $ex->getMessage();
}
try {
    $obj_post->post_image = $_file['post_image'];
} catch (Exception $ex) {
    $errors['post_image'] = $ex->getMessage();
}
try {
    $obj_post->post_tags = $_POST['post_tags'];
} catch (Exception $ex) {
    $errors['post_tags'] = $ex->getMessage();
}
if(count($errors) == 0)
{
    
    try {
        if(isset($_SESSION['post_key']))
        {
            $post_key = $_SESSION['post_key'];
        }
        $obj_post->update_post($post_key);
        $msg = "<span class='alert alert-success' style='height:30px!important; padding-left:10px!important; padding-right:10px!important; font-size:12px;'>Post successfully updated</span>";
        $_SESSION['msg'] = $msg;
        header("Location:../posts.php");
    } catch (Exception $ex) {
        echo $ex->getMessage();
    }
}
else
{
    if(isset($_SESSION['post_key']))
        {
            $post_key = $_SESSION['post_key'];
        }
    
        $msg = "<span class='alert alert-success' style='padding:0!important; padding-left:10px!important; padding-right:10px!important; font-size:12px;'>Post does't update</span>";
        $_SESSION['msg'] = $msg;
        header("Location:../update_post.php?post_key=$post_key");
        $_SESSION['obj_post'] = serialize($obj_post);
        $_SESSION['errors'] = $errors;
}
