<?php
session_start();
require_once '../models/Post.php';
$obj_post = new Post();
$errors = array();

//echo("<pre>");
//print_r($_POST['post_category']);
//echo ("</pre>");
//die;

try {
    $obj_post->post_name = $_POST['post_name'];
} catch (Exception $ex) {
    $errors['post_name'] = $ex->getMessage();
}

try {
    $obj_post->post_category = $_POST['post_category'];
} catch (Exception $ex) {
    $errors['post_category'] = $ex->getMessage();
}


try {
    $obj_post->post_description = $_POST['post_description'];
} catch (Exception $ex) {
    $errors['post_description'] = $ex->getMessage();
}
try {
    $obj_post->post_image = $_FILES['post_image'];
} catch (Exception $ex) {
    $errors['post_image'] = $ex->getMessage();
}
try {
    $obj_post->post_tags = $_POST['post_tags'];
} catch (Exception $ex) {
    $errors['post_tags'] = $ex->getMessage();
}
if(count($errors) == 0)
{
    try {
        $obj_post->add_post();
        $msg = "<span class='alert alert-success' style='height:30px!important; padding-left:10px!important; padding-right:10px!important; font-size:12px;'>Post successfully inserted</span>";
        $obj_post->upload_post_image($_FILES['post_image']['tmp_name']);
        $_SESSION['msg'] = $msg;
        header("Location:../insert_post.php");
    } catch (Exception $ex) {
        echo $ex->getMessage();
    }
}
else
{
    
        $msg = "<span class='alert alert-success' style='padding:0!important; padding-left:10px!important; padding-right:10px!important; font-size:12px;'>Post unsuccessfully inserted</span>";
        $_SESSION['msg'] = $msg;
        header("Location:../insert_post.php");
        $_SESSION['obj_post'] = serialize($obj_post);
        $_SESSION['errors'] = $errors;
}
