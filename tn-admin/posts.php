<?php
require_once 'views/top.php';
require_once 'models/Post.php';
?>
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
  <?php
require_once 'views/top_navbar.php';
  ?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <?php
  require_once 'views/left_sidebar.php';
  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
     <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>All Posts</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">View Posts</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <section class="content">
      <div class="container-fluid">
          <div class="row">
              <div class="col-md-12">
         <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">View all posts</h3>

                <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 300px;">
                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover">
                  <tr>
                    <th>ID</th>
                    <th>Date</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Views</th>
                    <th>Comments</th>
                    <th>Image</th>
                    <th>Action</th>
                  </tr>
                  <?php
                  $posts = Post::get_posts(0, 100);
                  foreach ($posts as $p)
                  {
                      ?>
                  <tr>
                    <td><?php echo($p->id);?></td>
                    <td><?php echo($p->post_date);?></td>
                    <td><?php echo($p->post_name);?></td>
                    <td><?php echo(substr($p->post_description, 0, 50));?>...</td>
                    
                    <td><?php echo($p->post_views);?></td>
                    <td><?php echo($p->post_comments);?></td>
                    <td><img style="width: 50px; height: 50px" src="<?php echo($p->post_image);?>"></td>
                    <td><a style="margin-right: 10px;" href="update_post.php?post_key=<?php echo($p->id);?>" class="btn btn-primary btn-sm">Edit</a><a href="controller/remove.php?action=remove_post&post_key=<?php echo($p->id)?>" class="btn btn-primary btn-sm">Delete</a></td>
                  </tr>
                  
                  <?php
                  }
                  ?>
                  
                   
                </table>
                  <!--Pagination-->
<nav aria-label="pagination">
    <ul class="pagination pagination-sm pg-blue">
        <!--Arrow left-->
        <li class="page-item disabled">
            <a class="page-link" href="#" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
                <span class="sr-only">Previous</span>
            </a>
        </li>
        <li class="page-item active">
            <a class="page-link" href="#">1 <span class="sr-only">(current)</span></a>
        </li>
        <li class="page-item"><a class="page-link" href="#">2</a></li>
        <li class="page-item"><a class="page-link" href="#">3</a></li>
        <li class="page-item"><a class="page-link" href="#">4</a></li>
        <li class="page-item"><a class="page-link" href="#">5</a></li>
        <li class="page-item">
            <a class="page-link" href="#" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
                <span class="sr-only">Next</span>
                </a>
            </li>
        </ul>
    </nav>
 </div>
              <!-- /.card-body -->
    </div>
            <!-- /.card -->
     </div>
</div>
          </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php
  require_once 'views/footer.php';

