<?php
//session_start();
require_once 'views/top.php';
require_once 'models/Post.php';
require_once 'models/Category.php';
?>
<style>
    #mytextarea
    {
        height: 250px;
    }
</style>
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
  <?php
    require_once 'views/top_navbar.php';
  ?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <?php
    require_once 'views/left_sidebar.php';
  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add New Post
                <?php
            if(isset($_SESSION['msg']))
            {
                echo $msg = $_SESSION['msg'];
                
                unset($_SESSION['msg']);
            }
            if(isset($_SESSION['errors']))
            {
                $error = $_SESSION['errors'];
                unset($_SESSION['errors']);
            }
            if(isset($_SESSION['obj_post']))
            {
                $obj_post = unserialize($_SESSION['obj_psot']);
            }
            else
            {
                $obj_post = new Post();
            }
           ?>
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Update Post 
              
              </li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
          <div class="card-header">
              <h3 class="card-title">Update Post </h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              
            </div>
          </div>
          <!-- /.card-header -->
          
          <?php
             try {
                 if(isset($_GET['post_key']))
                 {
            $_SESSION['post_key'] = $_GET['post_key'];
           $post_key = $_GET['post_key'];
          $post = Post::get_single_post($post_key);
          foreach ($post as $p)
          {
           ?>
          <div class="card-body">
              
              <form action="controller/update_post_process.php" method="post" enctype="multipart/form-data">
            <div class="row">
                
              <div class="col-md-6">
                <div class="form-group">
                  <label for="post_title">Update Post  
                      <span>
                          <?php
                          if(isset($error['post_name']))
                          {
                              echo $error['post_name'];
                          }
                          ?>
                      </span>
                  </label>
                    <input type="text" class="form-control" name="post_name" value="<?php echo($p->post_name); ?>" id="post_name" placeholder="Enter title here">
                </div>
                <!-- /.form-group -->
              </div>
                <div class="col-md-6">
                    <div class="form-group">
                       <label>Categories
                        <span>
                          <?php
                          if(isset($error['post_category']))
                          {
                              echo $error['post_category'];
                          }
                          ?>
                      </span>
                       </label>
                  <select class="form-control select2" name="post_category" style="width: 100%;">
                    <option>Select category</option>
                    
                    <?php
                    $categories = Category::get_categories();
                    foreach ($categories as $c)
                    {
                        ?>
                    <option value="<?php echo($c->id);?>"><?php echo ($c->category_name);?></option>
                    <?php
                    }
                    ?>
                  </select>
                </div>
                <!-- /.form-group -->
                </div>
              <!-- /.col -->
              <div class="col-md-12">
                  <div class="form-group">
                      <label for="mytextarea">Description 
                      <span>
                          <?php
                          if(isset($error['post_description']))
                          {
                              echo $error['post_description'];
                          }
                          ?>
                      </span>
                      </label>
                      <textarea class="form-control" id="mytextarea" name="post_description"></textarea>
                  </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="post_tags">Thumbnail image 
                  <span>
                          <?php
                          if(isset($error['post_image']))
                          {
                              echo $error['post_image'];
                          }
                          ?>
                      </span>
                  </label>
                  <input type="file" class="form-control" name="post_image" id="post_image">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                    <label>Tags 
                  <span>
                          <?php
                          if(isset($error['post_tags']))
                          {
                              echo $error['post_tags'];
                          }
                          ?>
                      </span>
                  </label>
                  <input type="text" class="form-control" name="post_tags" id="post_tags" placeholder="Enter post tags">
                </div>
              </div>
              <div class="col-md-6">
                  <input class="btn btn-primary" type="submit" value="Publish">
              </div>
                  
              <!-- /.col -->
            </div>
                  </form>
            <!-- /.row -->
          </div>
          <?php
          }
         }
          
             } catch (Exception $ex) {
                 echo $ex->getMessage();
             }
?>
          
          
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  </div>
  <!-- /.content-wrapper -->
<?php
  require_once 'views/footer.php';

