<?php
require_once 'views/top.php';
require_once 'models/Category.php';
?>
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
  <?php
require_once 'views/top_navbar.php';
  ?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <?php
  require_once 'views/left_sidebar.php';
  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
     <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-3">
            <h1>Add New Category</h1>
          </div>
            <div class="col-sm-3">
                <?php
                if(isset($_SESSION['obj_cat']))
                {
                    $obj_cat = unserialize($_SESSION['obj_cat']);
                }
                else
                {
                    $obj_cat = new Category();
                }
                    if(isset($_SESSION['msg']))
                    {
                        $msg = $_SESSION['msg'];
                        echo($msg);
                        unset($_SESSION['msg']);
                    }
                    if(isset($_SESSION['errors']))
                    {
                        $error = $_SESSION['errors'];
                        unset($_SESSION['errors']);
                        
                    }
                ?>
            </div>
            <div class="col-sm-3">
                <?php
                if(isset($_SESSION['rem_msg']))
                {
                    $rem_msg = $_SESSION['rem_msg'];
                    echo ($rem_msg);
                    unset($_SESSION['rem_msg']);
                }
                ?>
            </div>
          <div class="col-sm-3">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Categories</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <section class="content">
      <div class="container-fluid">
          <div class="row">
              <div class="col-md-5">
            <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Category
                
            </h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
         <form action="controller/add_category_process.php" method="post" enctype="multipart/form-data">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="category_name">Add New Category
                  <?php
                  if(isset($error))
                  {
                      echo $error['category_name'];
                  }
                  ?>
                  </label>
                    <input type="text" class="form-control" value="" id="category_name" name="category_name" placeholder="Enter category name here">
                </div>
                <!-- /.form-group -->
              </div>
              <div class="col-md-6">
                  <input class="btn btn-primary" type="submit" value="Add Category">
              </div>
                  
              <!-- /.col -->
            </div>
          </form>
            <!-- /.row -->
          </div>
          <!-- /.card-body -->
        </div>
              </div>

<div class="col-md-7">
         <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Categories</h3>

                <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 300px;">
                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover">
                  <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Date</th>
                    <th>Action</th>
                  </tr>
                  <?php
                  $cat = Category::get_categories();
                  foreach ($cat as $c)
                  {
                      ?>
                  <tr>
                    <td><?php echo($c->id);?></td>
                    <td><?php echo($c->category_name);?></td>
                    <td><?php echo($c->cat_date);?></td>
                    <td><a style="margin-right: 10px;" href="categories.php?edit_id=<?php echo($c->id);?>" class="btn btn-primary btn-sm">Edit</a><a href="controller/remove.php?action=remove_cat&key=<?php echo($c->id)?>" class="btn btn-primary btn-sm">Delete</a></td>
                  </tr>
                  <?php
                  }
                  ?>
                </table>
                  <!--Pagination-->
<nav aria-label="pagination">
    <ul class="pagination pagination-sm pg-blue">
        <!--Arrow left-->
        <li class="page-item disabled">
            <a class="page-link" href="#" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
                <span class="sr-only">Previous</span>
            </a>
        </li>
        <li class="page-item active">
            <a class="page-link" href="#">1 <span class="sr-only">(current)</span></a>
        </li>
        <li class="page-item"><a class="page-link" href="#">2</a></li>
        <li class="page-item"><a class="page-link" href="#">3</a></li>
        <li class="page-item"><a class="page-link" href="#">4</a></li>
        <li class="page-item"><a class="page-link" href="#">5</a></li>
        <li class="page-item">
            <a class="page-link" href="#" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
                <span class="sr-only">Next</span>
                </a>
            </li>
        </ul>
    </nav>
 </div>
              <!-- /.card-body -->
    </div>
            <!-- /.card -->
     </div>
</div>
              <div class="clearfix"></div>
<div class="col-md-5">
   <?php
  try {
    if(isset($_GET['edit_id']))
    {
        $edit_id = $_GET['edit_id'];
        $cat_data = Category::get_single_category($edit_id);
        foreach ($cat_data as $c_data)
        {
           ?>
    
    <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Update category</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
              <form action="controller/update_category_process.php?edit_id=<?php echo($edit_id);?>" method="post" enctype="multipart/form-data">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="category_name">Category name</label>
                  <input type="text" class="form-control" name="new_category_name"  value="<?php echo($c_data->category_name);?>" id="new_category_name" placeholder="Enter category new name here">
                </div>
                <!-- /.form-group -->
              </div>
              <div class="col-md-6">
                  <input class="btn btn-primary" type="submit" value="Update Category">
              </div>
                  
              <!-- /.col -->
            </div>
        </form>
            <!-- /.row -->
          </div>
          <!-- /.card-body -->
        </div>
    <?php
        }
        ?>
    
    <?php
    }
} catch (Exception $ex) {
    echo $ex->getMessage();
}

   ?>
    
   
</div>
</div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php
  require_once 'views/footer.php';

